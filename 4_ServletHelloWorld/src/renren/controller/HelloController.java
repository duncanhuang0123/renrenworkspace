package renren.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


// 可以用@WebServlet 來宣告路徑，或者是在web.xml下設定
// web.xml 可以在專案點右鍵 -> JAVA EE Tool -> Generate Deployment...  產生
//@WebServlet("/HelloController")
public class HelloController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public HelloController() {
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// 可以看到瀏覽器送過來的request header的內容
		System.out.println(request.getHeader("Accept-Encoding"));
		
		// 這個設定可以在瀏覽器端的Response Header看到
		response.setContentType("text/plain"); 
		response.setCharacterEncoding("utf-8");
		
		// 開始寫Response的文字，瀏覽器會收到這些文字，然後依照contentType來顯示
		response.getWriter().append("由  doGET 回覆").close();
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// 
//		response.setContentType("text/plain"); 
//		response.setCharacterEncoding("utf-8");
		
		response.getWriter().append("This is POST response 你好").close();
	}

}
