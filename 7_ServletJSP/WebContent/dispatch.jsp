<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>

<% System.out.println("I'm in console"); 
%>
<body>
	
	<h1>dispatch jsp</h1>
	<!-- expression language -->
	<h3> request attr : ${requestScope.someAttr} </h3>
	
	<h4>======================================================</h4>
	<% out.print(request.getAttribute("someAttr")); %>
	<%= request.getAttribute("someAttr") %>
	<h4>======================================================</h4>
	
	<h3> session attr : ${sessionScope.someAttr} </h3>
	
	<p>aaa + ${requestScope.strList}</p>
	
	
	
	<!--  this is bad practice -->
	<% 
	List<String> strList = (List) request.getAttribute("strList");
	for(String s: strList){ 
		out.print("<h2>"+ s +"</h2>");
	} %>
	
	<h1>*******************************************************</h1>
	
	<p>QQQ + ${requestScope.strList[0]}</p>
	
	<h3>&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&</h3>
	
	<h1>jstl tag， 記得下載 jstl jar 1.2</h1>
	<c:forEach items="${requestScope.strList}" var="str">
		<h3> ${str}</h3>
	</c:forEach>
	
</body>
</html>

<%-- <% request.getRequestDispatcher("/loginSuccess.jsp").forward(request, response); %> --%>
