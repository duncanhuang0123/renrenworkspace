1.	cmd 執行 Main1 指令
	
	cd 至 專案資料夾
	java -cp ./bin com.renren.main.Main1
	
2.	cmd 執行Main2指令
	
	cd 至 專案資料夾
	java -cp "./bin;./lib/*" com.renren.main.Main2  !!Unix系統分號要改成冒號
	
3.	-cp 是告訴java去哪裡找class檔，我們的class檔放在./bin裡面，所以-cp後面要有.bin
	然後因為Main2有用到其它jar檔，放在lib裡面，所以也要把它加進來
	
小結：eclipse其實就是在幫我們先用javac編譯java檔，把.java檔變成.class檔。.java檔的來源資料夾和.class檔的目的資料夾可以在 
	 專案點右鍵 -> Build Path -> Configure Build Path -> Source 設定
	.class檔都做好了之後，eclipse幫我們下 java指令執行.class檔，加jar檔是告訴eclipse在下"java"指令時，要把jar檔加到-cp下面