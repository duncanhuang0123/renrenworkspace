package com.renren.main;

import org.apache.commons.lang.StringUtils;

public class Main2 {
	
	public static void main(String args[]){
		
		// capitalize() 會把第一個字母變大寫
		String hi = StringUtils.capitalize("hello world");
		
		System.out.println(hi);
	}
	
}
