package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Person;
import util.DBUtil;

// DataAccessObject
public class PersonDao {
	
	public int insert(Person person) {
		
		// 跟database連線
		Connection conn = DBUtil.getConn();
		
		// 產生sql
		// EX. INSERT INTO PERSON(NAME, AGE, GENDER) VALUES('Trump', 72, 'M')
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO PERSON(NAME, AGE, GENDER) VALUES(");
		sql.append("'" + person.getName() + "',");
		sql.append(person.getAge() + ",");
		sql.append("'" + person.getGender() + "'");
		
		sql.append(")");
		
		Statement stmt = null;
		int rowsAffected = 0;
		
		try {
			
			stmt = conn.createStatement();
			// 用Statement物件執行sql指令
			// 取得插入的筆數，因為是插入，rowsAffected應該是1
			rowsAffected = stmt.executeUpdate(sql.toString());
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			// 關閉資源
			DBUtil.CloseConn(conn, stmt, null);
		}

		return rowsAffected;
	}

	public int update(Person person) {
		// UPDATE PERSON SET NAME=?, AGE=?, GENDER=? WHERE ID=?
		return 0;
	}

	public int delete(int personId) {
		
		// 跟database連線
		Connection conn = DBUtil.getConn();
		
		// 產生sql
		// EX. DELETE FROM PERSON WHERE ID=1
		StringBuffer sql = new StringBuffer();
		sql.append("DELETE FROM PERSON WHERE ID=?");
		
		PreparedStatement pstmt = null;
		int rowsAffected = 0;
		
		try {
			
			pstmt = conn.prepareStatement(sql.toString());
			pstmt.setInt(1, personId);
			
			rowsAffected = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			// 關閉資源
			DBUtil.CloseConn(conn, pstmt, null);
		}

		return rowsAffected;
		
	}

	public List<Person> getAll() {
		
		// 跟database連線
		Connection conn = DBUtil.getConn();
		
		// 產生sql
		// EX. SELECT * FROM PERSON
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT * FROM PERSON");
		
		Statement stmt = null;
		ResultSet rs = null;
		List<Person> result = new ArrayList<>();
		try {
			
			stmt = conn.createStatement();
			// 用Statement物件執行sql指令
			rs = stmt.executeQuery(sql.toString());
			
			while(rs.next()){
				Person p = new Person();
				p.setId(rs.getInt("ID"));
				p.setName(rs.getString("NAME"));
				p.setAge(rs.getInt("AGE"));
				p.setGender(rs.getString("GENDER").toCharArray()[0]);
				result.add(p);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			// 關閉資源
			DBUtil.CloseConn(conn, stmt, rs);
		}
		
		return result;
	}

	public Person getById(int personId) {
		
		// SELECT * FROM PERSON WHERE ID=?
		
		return null;
	}
}
