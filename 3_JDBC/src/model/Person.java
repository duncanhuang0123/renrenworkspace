package model;

public class Person {
	
	private int id;
	
	private String name;
	
	private int age;
	
	private char gender;
	
	// 建構子是特殊的方法，沒有回傳值，是專門用來初始化物件的方法，在 new 物件的時候會自動執行這個方法
	public Person(String name, int age, char gender) {
		super();
		// this 代表"本身這個物件"
		this.name = name;
		this.age = age;
		this.gender = gender;
	}
	
	public Person() {
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}

	public void introduce(){
		System.out.println("大家好，我是" + this.name);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", name=" + name + ", age=" + age + ", gender=" + gender + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (id != other.id)
			return false;
		return true;
	}


	
	
	
}
