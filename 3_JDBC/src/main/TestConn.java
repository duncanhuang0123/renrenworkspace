package main;

import java.sql.Connection;
import java.sql.DriverManager;

public class TestConn {

	public static void main(String args[]) throws Exception {

		// 註冊driver
		Class.forName("com.mysql.jdbc.Driver");

		// 取得連線
		Connection conn = DriverManager
				// 這是 mysql connector string
				.getConnection("jdbc:mysql://localhost/renren", "root", "redhat");
		
		Connection conn2 = DriverManager
				// 這是 mysql connector string
				.getConnection("jdbc:mysql://localhost/renren", "root", "redhat");
		
		System.out.println(conn);
		
		System.out.println(conn2);

		conn.close();

	}

}
