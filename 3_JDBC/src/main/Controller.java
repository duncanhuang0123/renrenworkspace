package main;

import java.util.List;

import dao.PersonDao;
import model.Person;

public class Controller {

	
	public static void main(String[] args) {
		
		PersonDao personDao = new PersonDao();
		
//		Person person = new Person("Trump", 72, 'M');
//		
//		int rowsAffected = personDao.insert(person);
//		
//		System.out.println(rowsAffected);
		
		
		List<Person> personList = personDao.getAll();
		
		Person person = personList.get(0);
		
		person.setName("NewName");
		
		personDao.update(person);
	}

	
}
