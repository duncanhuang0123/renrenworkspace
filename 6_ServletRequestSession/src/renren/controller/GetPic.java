package renren.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class GetPic
 */
@WebServlet("/GetPic")
public class GetPic extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetPic() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		System.out.println("attribute 'user' in request: "+ request.getAttribute("user"));
		System.out.println("attribute 'user' in session: "+ session.getAttribute("user"));
		
		if(session.getAttribute("user") == null)
			return;
		
		File f = new File("F:/desktop/renrenworkspace/6_ServletRequestSession/src/image.jpg");
		
		FileInputStream fis = new FileInputStream(f);
		
		ServletOutputStream outputStream = response.getOutputStream();
		
		int intRead = 0;
		while((intRead = fis.read()) != -1){
			outputStream.write(intRead);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
