package renren.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import renren.bo.LoginBo;
import renren.model.User;

/**
 * Servlet implementation class HelloController
 */
@WebServlet("/Login")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public LoginController() {
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		this.doPost(request, response);

		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		LoginBo loginBo = new LoginBo();
		
		if(!loginBo.login(username, password))
			return;
		
		System.out.println(username);
		System.out.println(password);
		
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		
		request.getSession().setAttribute("user", user);
		
		// 只是為了要講解request和session的不同
		request.setAttribute("user", user);
		
		response.getWriter().append("log in OK").close();
		
	}

}
