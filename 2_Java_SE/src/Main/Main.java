package Main;

import Model.Person;
import Storage.PersonStorage;
import Util.DateUtil;

public class Main {
	
	public static void main(String args []){
		
		// 參數可以在 Main.java 點右鍵-> run as -> run configuration-> arguments 下面設定
		// 先判斷長度大於零0的話
		if(args.length>0){
			// 把參數印出來
			for (String arg : args) {
				System.out.println(arg);
			}
			
		}
		
		
		// 物件概念
		
		// 做出1個人
		Person hilary = new Person();
		
		// 可以看到String的預設值是null，int是0，char是' '空白
		System.out.println(hilary);
		
		hilary.setName("Hilary");
		hilary.setGender('F');
		hilary.setAge(70);
		hilary.introduce();
		
		// 做出另外一個人
		Person trump = new Person("Trump", 87, 'M');
		trump.introduce();
		
		// 做出另外一個內容與trump相同的物件
		Person trump2 = new Person("Trump", 87, 'M');
		trump.introduce();
		
		// 比記憶體位置
		System.out.println(trump == trump2);
		
		// 比實際內容，需要Overwrite Person 的 equals() 和 hashCode()
		System.out.println(trump.equals(trump2));
		// 動態的東西都是跟著物件(instance)的
		
		// 靜態的東西是跟著類別(class)的，靜態的東西在java一起動的時候就會做好了
		String mingDuoDateStr = DateUtil.dateStrToMingGuoDateStr("2016/10/09");
		System.out.println(mingDuoDateStr);
		
		
		PersonStorage.person.introduce();
		
		// 小結： 動態的東西都是跟著物件的，若要執行動態的方法，一定要有物件存在，要嘛自己new，要嘛跟別人拿。
		//		靜態的東西在java一起動的時候就都做好了，所以在專案的任何地方都可以拿得到。
		
	}
	
}
