package Util;

public class DateUtil {
	
	/**
	 *   input sould be 2000/01/01 or 2000-01-01
	 *   return 89/01/01
	 */
	public static String dateStrToMingGuoDateStr(String dateStr){
		
		if(dateStr == null)
			return "";
		
		String [] dateStrParts = dateStr.split("[-/]");
		
		if(dateStrParts.length != 3)
			return "";
		
		int year = Integer.parseInt(dateStrParts[0]);
		String month = dateStrParts[1];
		String day = dateStrParts[2];
		return year-1911 + "/" + month + "/" + day;
		
	}
	
}
