package duncan.listener;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Application Lifecycle Listener implementation class CurrentUserListener
 *
 */
@WebListener
public class CurrentUserListener implements HttpSessionListener {

    /**
     * Default constructor. 
     */
    public CurrentUserListener() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see HttpSessionListener#sessionCreated(HttpSessionEvent)
     */
    public void sessionCreated(HttpSessionEvent event)  { 
    	HttpSession session = event.getSession();
    	
    	ServletContext servletContext = session.getServletContext();
    	
    	Integer currentUserCount = (Integer) servletContext.getAttribute("currentUserCount");
    	
    	System.out.println("session Listener: "+ currentUserCount);
    	currentUserCount++;
    	
    	servletContext.setAttribute("currentUserCount", currentUserCount);
    	
    }

	/**
     * @see HttpSessionListener#sessionDestroyed(HttpSessionEvent)
     */
    public void sessionDestroyed(HttpSessionEvent event)  { 
         
    	HttpSession session = event.getSession();
    	
    	ServletContext servletContext = session.getServletContext();
    	
    	Integer currentUserCount = (Integer) servletContext.getAttribute("currentUserCount");
    	
    	System.out.println("session Listener: "+ currentUserCount);
    	currentUserCount--;
    	
    	servletContext.setAttribute("currentUserCount", currentUserCount);
    	
    }
	
}
