package duncan.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServletRequest;

/**
 * Application Lifecycle Listener implementation class AccessedTimesListener
 *
 */
@WebListener
public class AccessedTimesListener implements ServletRequestListener {

    /**
     * Default constructor. 
     */
    public AccessedTimesListener() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see ServletRequestListener#requestDestroyed(ServletRequestEvent)
     */
    public void requestDestroyed(ServletRequestEvent arg0)  { 
         // TODO Auto-generated method stub
    }

	/**
     * @see ServletRequestListener#requestInitialized(ServletRequestEvent)
     */
    public void requestInitialized(ServletRequestEvent event)  { 
         
    	HttpServletRequest request = (HttpServletRequest)event.getServletRequest();
    	
    	ServletContext context = request.getServletContext();
    	
    	Integer requestedTime = (Integer)context.getAttribute(request.getRequestURI());
    	
    	requestedTime = requestedTime == null ? 0 : requestedTime;
    	
    	requestedTime++;
    	
    	System.out.println(requestedTime);
    	
    	System.out.println(request.getRequestURI());
    	
    	context.setAttribute(request.getRequestURI(), requestedTime);
    	
    	
    }
	
}
