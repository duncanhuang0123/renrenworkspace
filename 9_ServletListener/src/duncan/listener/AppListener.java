package duncan.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Application Lifecycle Listener implementation class AppListener
 *
 */
@WebListener
public class AppListener implements ServletContextListener {

    /**
     * Default constructor. 
     */
    public AppListener() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent arg0)  { 
         // TODO Auto-generated method stub
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent event)  { 
         System.out.println("initailized");
         
         ServletContext servletContext = event.getServletContext();
         String initParameter = servletContext.getInitParameter("contextParam1");
         System.out.println(initParameter);
         
         servletContext.setAttribute("contextAttr", "YOYOYO");
         
         servletContext.setAttribute("currentUserCount", new Integer(0));
         
         
    }
	
}
