環境建置
1. 下載 tomcat 8，解壓縮 
2. 在 window -> preference -> server ->runtime environment 建立一個新server 
3. 設定專案的facet。 專案點右鍵 -> properties -> project facets -> Runtimes 勾選server
4. 若要執行，直接在專案上點 run on server 即可


請嘗試手動部屬至tomcat
1. 利用產生war檔
2. 將war檔放至 tomcat目錄下的webapp內
3. 在tomcat/bin目錄下，手動啟動tomcat